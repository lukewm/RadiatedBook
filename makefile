 # ___ _  _ ____   ____ ____ ___  _ ____ ___ ____ ___    ___  ____ ____ _  _
 #  |  |--| |===   |--< |--| |__> | |--|  |  |=== |__>   |==] [__] [__] |-:_
allwikipages=$(filter-out texts-wiki/book.wiki, $(shell ls texts-wiki/*.wiki))
textshtml=texts-html/*.html
htmlpages=$(shell ls texts-html/*.html)
htmlbook=book.html
htmltmp=tmp.html # to receive the render js
pdfbook=book.pdf
pdfcss=resources/style.pdf.css
mode=remote #gets overwriten if make is run with argument mode=local

test: 
	@echo "testing makefile"
	@for i in $(allwikipages) ; \
	do echo $$i; \
	done

folders:
	mkdir -p texts-html 
	mkdir -p texts-html/imgs

download: cleanhtmlsrc
	@echo "downloading wikipages as html" ; \
	python ./download_wiki.py 

# TO DO - Instead of cleaning src can script see if update is necessary??

download_imgs: 
	@echo "downloading images from wikipages onto imgs" ; \
	python ./download_wiki.py --download=images


book.html: clean 
	@echo "manipulating html source files to make book.html" $(mode) ; \
        python html_manipulate.py --mode $(mode)

book.pdf: #book.html 
	phantomjs render.js > $(htmltmp) 
	weasyprint $(htmltmp) -s $(pdfcss) $(pdfbook)

clean:  # remove outputs
	@echo "cleaning outputs"	
	@rm -f $(htmlbook)

cleanhtmlsrc: 
	@echo "cleaning html source files"	
	@rm -f $(textshtml)

cleanwikipages:
	@rm -f texts-wiki/book.wiki
